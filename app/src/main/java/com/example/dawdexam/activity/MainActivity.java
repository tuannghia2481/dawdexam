package com.example.dawdexam.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dawdexam.R;
import com.example.dawdexam.database.DBHelper;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edName;
    private EditText edQuantity;
    private Button btAdd;
    private Button btView;
    private DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        db = new DBHelper(this);
        db.getReadableDatabase();
    }

    private void initView() {
        edName = findViewById(R.id.edName);
        edQuantity = findViewById(R.id.edQuantity);
        btAdd = findViewById(R.id.btAdd);
        btView = findViewById(R.id.btView);
        btAdd.setOnClickListener(this);
        btView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btAdd) {
            onAdd();
        }
        if (v == btView) {
            onView();
        }

    }

    private void onAdd() {
        if (edName.getText().toString().isEmpty() || edQuantity.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter product name and quantity", Toast.LENGTH_LONG).show();
            return;
        }

        String isAdd = db.addProduct(edName.getText().toString(), Integer.parseInt(edQuantity.getText().toString()));
        Toast.makeText(this, isAdd, Toast.LENGTH_LONG).show();
    }

    private void onView() {
        Intent intent = new Intent(MainActivity.this, ListProductAct.class);
        startActivity(intent);
    }
}